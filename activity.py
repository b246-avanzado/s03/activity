
# Activity:
# 1. Create a Class called Camper and give it the attributes name, batch, course_type
# 2. Create a method called career_track which will print out the string Currently enrolled in the <value of course_type> program.
# 3. Create a method called info which will print out the string My name is <value of name> of batch <value of batch>.
# 4. Create an object from class Camper called zuitt_camper and pass in arguments for name, batch, and course_type.
# 5. Print the value of the object's name, batch, course type.
# 6. Execute the info method and career_track of the object.

class Camper():
    def __init__(self, name, batch, course_type):
        self.name = name
        self.batch = batch
        self.course_type = course_type

    def career_track(self):
        print(f"Currently enrolled in the {self.course_type} program.")

    def info(self):
        print(f"My name is {self.name} of batch {self.batch}.")

camper1 = Camper("Alan", 100, "python short course")
print(f"Camper Name: {camper1.name}")
print(f"Camper Batch: {camper1.batch}")
print(f"Camper Course: {camper1.course_type}")

camper1.info()
camper1.career_track()